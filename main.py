import spidev
from argparse import ArgumentParser

class AD5791:
    REGADDR = 20

    DAC_REG = 1

    CONTROL_REG = 2
    RBUF = 1
    OPGND = 2
    DACTRI = 3
    BIN_2SC = 4
    SDODIS = 5
    LIN_COMP = 6

    SOFT_CTRL_REG = 4
    LDAC = 0
    CLR = 1
    RESET = 2

    def _writereg(self, data):
        self._dev.writebytes(data.to_bytes(3, 'big'))

    def _readreg(self, reg):
        self._writereg(reg << self.REGADDR)
        return self._dev.readbytes(3)

    def initialize(self):
        self._writereg(self.CONTROL_REG << self.REGADDR | 1 << self.BIN_2SC)

    def __init__(self, dev):
        self._dev = dev
        self.initialize()

    def set(self, code):
        self._writereg(self.DAC_REG << self.REGADDR | code)

if __name__ == '__main__':
    p = ArgumentParser()
    p.add_argument('voltage', type=float)
    args = p.parse_args()

    device = spidev.SpiDev(0, 0)
    device.max_speed_hz = 500000
    device.mode = 2

    code = round((args.voltage + 5) * 1048575 / 10)

    print(code)
    AD5791(device).set(code)
